import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SinglePageComponent } from './pages/single-page/single-page.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { 
        path: '', 
        component: SinglePageComponent
      },
      // { 
      //   path: '**', 
      //   redirectTo: ''
      // }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
