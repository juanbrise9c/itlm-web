import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicRoutingModule } from './public-routing.module';
import { SwiperModule } from 'swiper/angular';
import { SinglePageComponent } from './pages/single-page/single-page.component';
import { ViewImageComponent } from './pages/view-image/view-image.component';


@NgModule({
  declarations: [
    SinglePageComponent,
    ViewImageComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    SwiperModule
  ]
})
export class PublicModule { }
