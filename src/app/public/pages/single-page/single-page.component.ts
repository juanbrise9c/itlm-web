import { Component, OnInit } from '@angular/core';
import { SwiperComponent } from "swiper/angular";

import SwiperCore, { Autoplay, Navigation, Pagination, SwiperOptions } from "swiper";
import { MatDialog } from '@angular/material/dialog';
import { ViewImageComponent } from '../view-image/view-image.component';
import { ApiService } from '../../../core/services/api/api.service';
import { AuthService } from '../../../core/services/auth/auth.service';
import * as moment from 'moment';
import { GeneralFunctions } from '../../../core/services/general-functions';

SwiperCore.use([Autoplay, Pagination, Navigation]);

@Component({
  selector: 'app-single-page',
  templateUrl: './single-page.component.html',
  styleUrls: ['./single-page.component.scss']
})
export class SinglePageComponent implements OnInit {

  constructor(
    private _apiService: ApiService,
    public _authService: AuthService,
    private _dialog: MatDialog,
    public _generalFunctions: GeneralFunctions
  ) { }

  public loading: boolean = false;
  public career: any;
  public advertisements: any = [];
  public posts: any = [];
  public teachers: any = [];

  ngOnInit(): void {
    this.getCareer();
  }

  config: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween : 50,
    navigation   : true,
    autoplay     : {delay: 2500,disableOnInteraction: false},
    pagination   : { clickable: true },
  };

  getCareer() {
    const query = this._apiService.getSearchQuery('Informática', ['name']);
    const filter = {
      where: {
        and: [query]
      }
    };

    this._apiService.getDataObjects('Careers', filter)
    .then((data: any) => {
      if (data && data.length) this.career = data[0];
      this.getAdverstiments();
    })
    .catch((err: any) => {
      this.loading = false;
    })
  }

  getAdverstiments() {
    const filter = {
      where: {
        careerId: this.career.id,
        untilDate: { gt: new Date() },
        status: 'online'
      }
    };

    this._apiService.getDataObjects('Publicitys', filter)
    .then((data: any) => {
      if (data && data.length) this.advertisements = data;
      this.getPosts();
    })
    .catch((err: any) => {
      this.loading = false;
    })
  }

  getPosts() {
    const filter = {
      where: {
        careerId: this.career.id,
        // untilDate: { gt: new Date() },
        status: 'online'
      }
    };

    this._apiService.getDataObjects('Posts', filter)
    .then((data: any) => {
      if (data && data.length) this.posts = data;
      this.getTeachers();
    })
    .catch((err: any) => {
      this.loading = false;
    })
  }

  getTeachers() {
    const filter = {
      where: {
        careerId: this.career.id,
        status: 'online'
      }
    };

    this._apiService.getDataObjects('Teachers', filter)
    .then((data: any) => {
      if (data && data.length) this.teachers = data;
      this.loading = false;
    })
    .catch((err: any) => {
      this.loading = false;
    })
  }

  openData(data: any): void {
    const dialogRef = this._dialog.open(ViewImageComponent, {
      width: '90%',
      data: {...data, viewImage: true},
      panelClass: 'imagePanel'
    });
  }

  openPost(data: any): void {
    const dialogRef = this._dialog.open(ViewImageComponent, {
      width: '800px',
      data: data,
      panelClass: 'postPanel'
    });
  }

  formateDate(date: any) {
    return moment(date).locale('Es').format('LL');
  }
}
