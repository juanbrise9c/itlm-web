import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '../../../core/services/auth/auth.service';

@Component({
  selector: 'app-view-image',
  templateUrl: './view-image.component.html',
  styleUrls: ['./view-image.component.scss']
})
export class ViewImageComponent implements OnInit {

  constructor(
    public _authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
  ) { }

  ngOnInit(): void {
    if (!this.dataDialog.viewImage) {
      console.log('entro');
      document.getElementById('#content-html')!.innerHTML = '<b>HOla</b>';
    }
  }

}
