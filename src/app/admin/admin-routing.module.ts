import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessGuard } from '../core/guards/access.guard';
import { LoginComponent } from './modules/auth/pages/login/login.component';
import { StructureComponent } from './modules/home/pages/structure/structure.component';
import { TeachersComponent } from './modules/home/pages/teachers/teachers.component';
import { AdvertisementsComponent } from './modules/home/pages/advertisements/advertisements.component';
import { DashboardComponent } from './modules/home/pages/dashboard/dashboard.component';
import { PostsComponent } from './modules/home/pages/posts/posts.component';
import { CareersComponent } from './modules/home/pages/careers/careers.component';
import { AdminsComponent } from './modules/home/pages/admins/admins.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { 
        path: 'login',
        component: LoginComponent
      },
      { 
        path: '', 
        component: StructureComponent,
        canActivate: [AccessGuard],
        children: [
          {
            path: 'dashboard',
            component: DashboardComponent
          },
          {
            path: 'teachers',
            component: TeachersComponent
          },
          {
            path: 'advertisements',
            component: AdvertisementsComponent
          },
          {
            path: 'posts',
            component: PostsComponent
          },
          {
            path: 'careers',
            component: CareersComponent
          },
          {
            path: 'admins',
            component: AdminsComponent
          },
        ]
      },
      { 
        path: '**', 
        redirectTo: 'login'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
