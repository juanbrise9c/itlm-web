import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StructureComponent } from './pages/structure/structure.component';
import { MaterialModule } from '../../../shared/material/material.module';
import { TeachersComponent } from './pages/teachers/teachers.component'
import { RouterModule } from '@angular/router';
import { TeacherComponent } from './pages/teachers/teacher/teacher.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FilePondModule } from 'ngx-filepond';
import { AdvertisementsComponent } from './pages/advertisements/advertisements.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { PostsComponent } from './pages/posts/posts.component';
import { PostComponent } from './pages/posts/post/post.component';
import { CareersComponent } from './pages/careers/careers.component';
import { AdminsComponent } from './pages/admins/admins.component';
import { AdminComponent } from './pages/admins/admin/admin.component';


@NgModule({
  declarations: [
    StructureComponent,
    TeachersComponent,
    TeacherComponent,
    AdvertisementsComponent,
    DashboardComponent,
    PostsComponent,
    PostComponent,
    CareersComponent,
    AdminsComponent,
    AdminComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    FilePondModule,
    AngularEditorModule
  ]
})
export class HomeModule { }
