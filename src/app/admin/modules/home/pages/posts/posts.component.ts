import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ApiService } from '../../../../../core/services/api/api.service';
import { AuthService } from '../../../../../core/services/auth/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PostComponent } from './post/post.component';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  constructor(
    private _apiService: ApiService,
    public _authService: AuthService,
    private _router: Router,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  public searchbar = new FormControl({ value: '', disabled: false });
  public searchPage: number  = 0;
  public loading:    boolean = false;
  public posts:      any     = [];
  public status:     string  = 'online';
  public user:       any;

  ngOnInit(): void {
    // Get User ID
    this.user = this._authService.getToken().user;
    // Get Data
    this.loadData('');
  }

  ngAfterViewInit(): void {
    this.searchbar.valueChanges.pipe(debounceTime(250)).subscribe((data) => {
      this.loadData(data, true)
    });
  }

  loadData(dataSeach?: string, reset?: any, scroll?: any): void {
    this.loading = true;

    const andArray: any = [
      { careerId: this.user.careerId },
      { status: this.status }
    ];

    if (dataSeach) andArray.push(this._apiService.getSearchQuery(dataSeach, ['prefix', 'name', 'email', 'phone', 'description']));
    // set query properties 
    if (reset || !scroll) this.searchPage = 0;
    const page = 25;
    const skip = page * this.searchPage;

    const filter: any = {
      where: { and: andArray },
      include: ['career', 'createdBy'],
      limit: page,
      skip: skip,
      order: 'createdAt DESC'
    };

    this._apiService.getDataObjects('Posts', filter)
    .then((data: any) => {
      if (reset || !scroll) this.posts = data;
      else if (data.length && scroll) this.posts.push(...data);
      this.loading = false;
    })
    .catch((err: any) => {
      this.loading = false;
      console.log(err);
    })
  }

  openData(instance?: any): void {
    const data = {
      isNew: instance ? false : true,
      instance: instance
    };

    const dialogRef = this._dialog.open(PostComponent, {
      width: '800px',
      data: data,
      panelClass: 'panelModal'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.changes) this.loadData('');
    });
  }


  async perfomDelete(id: string) {
    try {
      const response: any = await this._apiService.deleteDataObject('Posts', id);
      this.openSnackBar('Eliminado correctamente.', 'toast-green');
      this.loadData('', true);
    } catch (error) {
      this.openSnackBar('No se ha podido eliminar correctamente.', 'toast-red');
    }
  }

  async perfomArchive(id: string) {
    try {
      const STATUS: any = {
        'archived': 'online',
        'online': 'archived'
      };

      const response: any = await this._apiService.editDataObject('Posts', id, { status: STATUS[this.status] });
      this.openSnackBar('Archivado correctamente.', 'toast-green');
      this.loadData('', true);
    } catch (error) {
      this.openSnackBar('No se ha podido archivar correctamente.', 'toast-red');
    }
  }

  openSnackBar(message: string, style: string) {
    this._snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: "end",
      verticalPosition: 'bottom',
      panelClass: [style]
    });
  }

  onScroll() {
    this.searchPage++;
    this.loadData('', false, true);
  }
}
