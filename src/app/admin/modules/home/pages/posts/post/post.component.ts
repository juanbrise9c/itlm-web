import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../../../../../core/services/auth/auth.service';
import { ApiService } from '../../../../../../core/services/api/api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from "../../../../../../../environments/environment";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  // HTML EDITOR
  public htmlContent = '';
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '250px',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Escribe...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ],
    customClasses: [],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['customClasses', 'insertImage', 'insertVideo']
    ]
  };
  // END HTML EDITOR

  // MY POND
  @ViewChild('myPond') myPond: any;
  public pondFiles: any = [];
  public pondOptions = {
    class: 'my-filepond',
    multiple: false,
    labelIdle: 'Arrastra tu imagen...',
    acceptedFileTypes: 'image/jpeg, image/png',
    limit: 1,
    server: {
      url: environment.url + '/Containers/itlm-web/upload?access_token=' + this._authService.getToken().id
    }
  };
  // END MY POND

  constructor(
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _apiService: ApiService,
    private _snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<PostComponent>,
    @Inject(MAT_DIALOG_DATA) public dataDialog: any
  ) { }

  public user: any = {};
  public isNew: boolean = false;
  public changes: boolean = false;
  public instanceId: string = "";

  form = this._fb.group({
    title: new FormControl({ value: '', disabled: false }, Validators.required),
    content: new FormControl({ value: '', disabled: false }),
    img: new FormControl({ value: '', disabled: false })
  });

  ngOnInit(): void {
    this.user = this._authService.getToken().user;
    this.isNew = this.dataDialog.isNew;
    this.instanceId = this.dataDialog.instance ? this.dataDialog.instance.id : null;

    if (!this.isNew) {
      this.htmlContent = this.dataDialog.instance.content;
      this.form.patchValue(this.dataDialog.instance);
    }
  }

  async perfomRequest() {
    try {
      if (this.form.invalid) return;

      const body = { 
        ...this.form.value,
        content: this.htmlContent,
        typePost: 'post'
      };

      let response: any;

      if (this.isNew) {
        body.createdById = this.user.id;
        body.careerId = this.user.careerId;

        response = await this._apiService.postDataObject('Posts', body);
        this.openSnackBar('Creado con exito.', 'toast-green');
      } else {
        response = await this._apiService.editDataObject('Posts', this.instanceId, body);
        this.openSnackBar('Editado con exito.', 'toast-green');
      }

      if (response && response.id) {
        this.changes = true;
        this.closeDialog();
      }
    } catch (error) {
      this.openSnackBar('No se ha podido crear con exito.', 'toast-red');
    }
  }

  onFileUploaded(event: any) {
    try {
      const response = JSON.parse(event.pond.getFiles()[0].serverId);
      const fileName = response ? response.result.files.perfil[0].name : '';
      this.form.patchValue({ img: fileName })
    } catch (error) {
      console.log(error);
    }
  }

  openSnackBar(message: string, style: string) {
    this._snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: "end",
      verticalPosition: 'bottom',
      panelClass: [style]
    });
  }

  closeDialog() {
    this.dialogRef.close({ changes: this.changes });
  }
}
