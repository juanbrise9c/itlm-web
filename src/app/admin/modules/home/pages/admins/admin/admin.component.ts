import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../../../../../core/services/auth/auth.service';
import { ApiService } from '../../../../../../core/services/api/api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { debounceTime, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public careerOptions!: Observable<any>;

  constructor(
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _apiService: ApiService,
    private _snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<AdminComponent>,
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
  ) { }

  public user: any = {};
  public isNew: boolean = false;
  public changes: boolean = false;
  public instanceId: string = "";

  form = this._fb.group({
    career: new FormControl({ value: '', disabled: false }, Validators.required),
    username: new FormControl({ value: '', disabled: false }, Validators.required),
    email: new FormControl({ value: '', disabled: false }, Validators.required),
    password: new FormControl({ value: '', disabled: false }, this.dataDialog.isNew ? Validators.required : null),
    name: new FormControl({ value: '', disabled: false }),
    phone: new FormControl({ value: '', disabled: false })
  });

  ngOnInit(): void {
    this.user = this._authService.getToken().user;
    this.isNew = this.dataDialog.isNew;
    this.instanceId = this.dataDialog.instance ? this.dataDialog.instance.id : null;

    if (!this.isNew) this.form.patchValue(this.dataDialog.instance);
  }

  ngAfterViewInit(): void {
    this.careerOptions = this.form.get('career')!.valueChanges.pipe(
      debounceTime(250),
      startWith(''),
      switchMap(value => this.loadCareerOptions(value))
    );
  }

  async loadCareerOptions(dataSearch: string) {
    const searchQuery = this._apiService.getSearchQuery(dataSearch, ['name', 'location']);
    const getQuery = {
      where: {
        searchQuery,
        status: "online"
      },
      limit: 25
    };

    const data: any = await this._apiService.getDataObjects('Careers', getQuery);
    return data && data.length ? data : [];
  }


  displayFn(item?: any): string {
    return item && item.name ? item.name : '';
  }

  async perfomRequest() {
    try {
      if (this.form.invalid) return;

      let response: any;
      const body = { ...this.form.value };

      delete body.career;
      body.careerId = this.form.get('career')!.value ? this.form.get('career')!.value.id : null;

      if (this.isNew) {
        body.createdById = this.user.id;
        response = await this._apiService.postDataObject('Admins', body);
        this.openSnackBar('Creado con exito.', 'toast-green');
      } else {
        delete body.password;
        response = await this._apiService.editDataObject('Admins', this.instanceId, body);
        this.openSnackBar('Editado con exito.', 'toast-green');
      }

      if (response && response.id) {
        this.changes = true;
        this.closeDialog();
      }
    } catch (error) {
      this.openSnackBar('No se ha podido crear con exito.', 'toast-red');
    }
  }

  openSnackBar(message: string, style: string) {
    this._snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: "end",
      verticalPosition: 'bottom',
      panelClass: [style]
    });
  }

  closeDialog() {
    this.dialogRef.close({ changes: this.changes });
  }
}
