import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../../core/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-structure',
  templateUrl: './structure.component.html',
  styleUrls: ['./structure.component.scss']
})
export class StructureComponent implements OnInit {

  constructor(
    private _authService: AuthService,
    private _router: Router
  ) { }

  user: any;
  isSuperUser: any;
  tmpRoutes: any;
  title: any;

  ngOnInit(): void {
    this.user = this._authService.getToken().user;
    this.isSuperUser = this.user.isSuperAdmin;
    this._authService.updateToken();
    this.tmpRoutes = (this.isSuperUser) ? [...this.superAdminMenu] : [...this.menu];
    this.title = (this.isSuperUser) ? 'SUPER ADMIN' : 'PANEL ADMIN';
  }

  public menu = [
    { title: 'Inicio',         route: '/dashboard',      icon: 'home'},
    { title: 'Maestros',       route: '/teachers',      icon: 'people'},
    { title: 'Anuncios',       route: '/advertisements', icon: 'hide_image'},
    { title: 'Publicaciones',  route: '/posts',         icon: 'photo_camera_front'},
    // { title: 'Actividades',    route: '/activities',    icon: 'engineering'},
    // { title: 'Oferta Laboral', route: '/jobs',          icon: 'work_outline'}
  ];

  public superAdminMenu = [
    { title: 'Carreras',        route: '/careers', icon: 'class'},
    { title: 'Administradores', route: '/admins',  icon: 'admin_panel_settings'}
  ];

  navigatTo(route: string): void {
    this._router.navigate([route]).catch();
  }

  logOut(): void {
    localStorage.removeItem('user_token');
    this.navigatTo('admin/login');
  }
}
