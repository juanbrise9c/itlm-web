import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../../core/services/api/api.service';
import { AuthService } from '../../../../../core/services/auth/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.scss']
})
export class CareersComponent implements OnInit {
    constructor(
      private _apiService: ApiService,
      public _authService: AuthService,
      private _router: Router,
      private _dialog: MatDialog,
      private _snackBar: MatSnackBar,
    ) { }
  
    public searchbar = new FormControl({ value: '', disabled: false });
    public searchPage: number = 0;
    public careers:   any    = [];
    public loading: boolean   = false;
    public status:     string = 'online';
    public user:       any;
  
    ngOnInit(): void {
      // Get User ID
      this.user = this._authService.getToken().user;
      // Get Data
      this.loadData('');
    }
  
    ngAfterViewInit(): void {
      this.searchbar.valueChanges.pipe(debounceTime(250)).subscribe((data) => {
        this.loadData(data, true)
      });
    }
  
    loadData(dataSeach?: string, reset?: any, scroll?: any): void {
      this.loading = true;
  
      const andArray: any = [
        { careerId: this.user.careerId },
        { status: this.status }
      ];
  
      if (dataSeach) andArray.push(this._apiService.getSearchQuery(dataSeach, ['name', 'mision', 'vision', 'email']));
      // set query properties 
      if (reset || !scroll) this.searchPage = 0;
      const page = 25;
      const skip = page * this.searchPage;
  
      const filter: any = {
        where: { and: andArray },
        include: ['createdBy'],
        limit: page,
        skip: skip,
        order: 'createdAt DESC'
      };
  
      this._apiService.getDataObjects('Careers', filter)
      .then((data: any) => {
        if (reset || !scroll) this.careers = data;
        else if (data.length && scroll) this.careers.push(...data);
        this.loading = false;
      })
      .catch((err: any) => {
        this.loading = false;
        console.log(err);
      })
    }
  
    openData(instance?: any): void {
      const data = {
        isNew: instance ? false : true,
        instance: instance
      };
  
      // const dialogRef = this._dialog.open('', {
      //   width: '700px',
      //   data: data,
      //   panelClass: 'panelModal'
      // });
  
      // dialogRef.afterClosed().subscribe(result => {
      //   if (result && result.changes) this.loadData('');
      // });
    }
  
    async perfomDelete(id: string) {
      try {
        const response: any = await this._apiService.deleteDataObject('Careers', id);
        this.openSnackBar('Eliminado correctamente.', 'toast-green');
        this.loadData('', true);
      } catch (error) {
        this.openSnackBar('No se ha podido eliminar correctamente.', 'toast-red');
      }
    }
  
    async perfomArchive(id: string) {
      try {
        const STATUS: any = {
          'archived': 'online',
          'online': 'archived'
        };
  
        const response: any = await this._apiService.editDataObject('Careers', id, { status: STATUS[this.status] });
        this.openSnackBar('Archivado correctamente.', 'toast-green');
        this.loadData('', true);
      } catch (error) {
        this.openSnackBar('No se ha podido archivar correctamente.', 'toast-red');
      }
    }
  
    openSnackBar(message: string, style: string) {
      this._snackBar.open(message, '', {
        duration: 3000,
        horizontalPosition: "end",
        verticalPosition: 'bottom',
        panelClass: [style]
      });
    }

    formateDate(date: any) {
      return moment(date).locale('Es').format('LL');
    }

    onScroll() {
      this.searchPage++;
      this.loadData('', false, true);
    }
}