import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { AuthService } from '../../../../../../core/services/auth/auth.service';
import { ApiService } from '../../../../../../core/services/api/api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from "../../../../../../../environments/environment.prod";

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.scss']
})
export class TeacherComponent implements OnInit {

  @ViewChild('myPond') myPond: any;

  pondFiles: any = [];
  pondOptions = {
    class: 'my-filepond',
    multiple: false,
    labelIdle: 'Arrastra tus imagenes...',
    acceptedFileTypes: 'image/jpeg, image/png',
    limit: 1,
    server: {
      url: environment.url + '/Containers/itlm-web/upload?access_token=' + this._authService.getToken().id
    }
  };

  constructor(
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _apiService: ApiService,
    private _snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<TeacherComponent>,
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
  ) { }

  public user: any = {};
  public isNew: boolean = false;
  public changes: boolean = false;
  public instanceId: string = "";

  form = this._fb.group({
    srcProfile: new FormControl({ value: '', disabled: false }),
    prefix: new FormControl({ value: '', disabled: false }),
    name: new FormControl({ value: '', disabled: false }, Validators.required),
    email: new FormControl({ value: '', disabled: false }, Validators.required),
    phone: new FormControl({ value: '', disabled: false }, Validators.required),
    description: new FormControl({ value: '', disabled: false })
  });

  ngOnInit(): void {
    this.user = this._authService.getToken().user;
    this.isNew = this.dataDialog.isNew;
    this.instanceId = this.dataDialog.instance ? this.dataDialog.instance.id : null;

    if (!this.isNew) this.form.patchValue(this.dataDialog.instance);
  }

  async perfomRequest() {
    try {
      if (this.form.invalid) return;

      const body = { ...this.form.value };
      let response: any;

      if (this.isNew) {
        body.createdById = this.user.id;
        body.careerId = this.user.careerId;

        response = await this._apiService.postDataObject('Teachers', body);
        this.openSnackBar('Creado con exito.', 'toast-green');
      } else {
        response = await this._apiService.editDataObject('Teachers', this.instanceId, body);
        this.openSnackBar('Editado con exito.', 'toast-green');
      }

      if (response && response.id) {
        this.changes = true;
        this.closeDialog();
      }
    } catch (error) {
      this.openSnackBar('No se ha podido crear con exito.', 'toast-red');
    }
  }

  openSnackBar(message: string, style: string) {
    this._snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: "end",
      verticalPosition: 'bottom',
      panelClass: [style]
    });
  }

  closeDialog() {
    this.dialogRef.close({ changes: this.changes });
  }

  onFileUploaded(event: any) {
    try {
      const response = JSON.parse(event.pond.getFiles()[0].serverId);
      const fileName = response ? response.result.files.perfil[0].name : '';
      this.form.patchValue({ srcProfile: fileName })
    } catch (error) {
      console.log(error);
    }
  }
}
