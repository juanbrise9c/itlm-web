import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../../../core/services/auth/auth.service';
import { ApiService } from '../../../../../core/services/api/api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _authService: AuthService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    const token = this._authService.getToken();
    if (token) this._router.navigate(['/admin/dashboard']).catch();
  }

  form = this._fb.group({
    username: new FormControl({ value: '', disabled: false}, Validators.required ),
    password: new FormControl({ value: '', disabled: false}, Validators.required )
  });

  perfomRequest() {
    this._authService.login(this.form.value)
    .then((data: any) => {
      if (data && data.id) {
        localStorage.setItem('user_token', btoa(JSON.stringify(data)));
        this._authService.updateToken();
        this.openSnackBar('Bienvenido');
        this._router.navigate(['/admin/dashboard']).catch();
      }
    })
    .catch((err: any) => {
      this.openSnackBar('Hay un problema con tu usuario');
      console.log(err);
    });
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: "end",
      verticalPosition: 'bottom'
    });
  }
}
