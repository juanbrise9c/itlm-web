import * as moment from 'moment';

export class GeneralFunctions  {
    processPhone(phone: any) {
        let formatedPhone = '';
        if (phone !== undefined && phone !== null && phone !== '') {
            if (typeof phone !== 'string') phone = phone.toString();
            let regex = /[^0-9\s]+/g;
            phone = phone.replace(regex, '').replace(/ /g, '').trim()
            if (phone !== '' && phone !== '0') {
                if (phone.length == 10) {
                    formatedPhone = '(' + phone.substring(0, 3) + ') ' + phone.substring(3, 6) + ' ' + phone.substring(6, 10);
                } else if (phone.length == 11) {
                    formatedPhone = (phone.substring(0, 1) + ' (' + phone.substring(1, 4) + ') ' + phone.substring(4, 7) + ' ' + phone.substring(7, 11))
                } else if (phone.length == 12) {
                    formatedPhone = (phone.substring(0, 2) + ' (' + phone.substring(2, 5) + ') ' + phone.substring(5, 8) + ' ' + phone.substring(8, 12))
                } else {
                    formatedPhone = phone;
                }
            }
        } else {
            formatedPhone = phone;
        }
        return formatedPhone;
    }
    
    
    formateDate(date: any) {
        return moment(date).locale('Es').format('LL');
    }
}
