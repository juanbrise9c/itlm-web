import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private _apiService: ApiService,
  ) { }

  url = environment.url;

  login(credentials: any) {
    return this.http.post(this.url + 'Admins/login?include=user', credentials).toPromise();
  }

  getToken(): any {
      let token = localStorage.getItem('user_token');
      if (token) token = JSON.parse(atob(token));
      return token;
  }

  updateToken(): any {
      const token = this.getToken();
      const filter = { include: ['career'] };
      this._apiService.getDataObject('Admins', token.userId, filter)
      .then((data: any) => {
        if (data) {
          token.user = data;
          localStorage.setItem('user_token', btoa(JSON.stringify(token)));
          this.setCareerConfig(token);
        }
      })
      .catch((err: any) => {
        console.log(err);
      })
  }

  setCareerConfig(token: any): void {
    const career = token.user && token.user.career ? token.user.career : '';
    if (!career) return;

    console.log(career);
    const style = document.documentElement.style;
    // Set System Color
    if (career.color) style.setProperty("--primary-color", career.color);
  }
}
