import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
  ) { }

  public url = environment.url;

  getToken() {
    const token = localStorage.getItem('user_token') || '';
    if (!token) return '';
    const user = JSON.parse(atob(token));
    return user ? user.id : '';
  }

  getDataObjects(model: string, filter: any) {
    return this.http.get(`${this.url}${model}?filter=${JSON.stringify(filter)}`, { headers:  new HttpHeaders({ Authorization: this.getToken() }) }).toPromise();
  }

  getDataObject(model: string, id: any, filter?: any) {
    const filterObject = filter ? '?filter=' + JSON.stringify(filter) : '';
    return this.http.get(`${this.url}${model}/${id}${filterObject}`, { headers:  new HttpHeaders({ Authorization: this.getToken() }) }).toPromise();
  }

  postDataObject(model: string, data: any) {
    return this.http.post(`${this.url}${model}`, data, { headers:  new HttpHeaders({ Authorization: this.getToken() }) }).toPromise();
  }

  editDataObject(model: string, id: string, data: any) {
    return this.http.patch(`${this.url}${model}/${id}`, data, { headers:  new HttpHeaders({ Authorization: this.getToken() }) }).toPromise();
  }

  deleteDataObject(model: string, id: string) {
    return this.http.delete(`${this.url}${model}/${id}`, { headers:  new HttpHeaders({ Authorization: this.getToken() }) }).toPromise();
  }

  getSearchQuery(dataSearch: string, properties: string[]) {
    if (dataSearch) {
      const orObject = [];

      if (typeof dataSearch === 'string')  {
        dataSearch = dataSearch.replace(/[#-.]|[[-^]|[?|{}]/g, '\\$&');
        for (const property of properties) {
            orObject.push({
              [property]: {
                  like: ('.*' + typeof dataSearch === 'string' ? dataSearch.trim() : dataSearch + '.*'),
                  options: 'i'
              }
            });
        }
        return { or: orObject };
      }
    }
    return {};
  }
}
