import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "../services/auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate {
  constructor(
    private _authService: AuthService,
    private _router: Router
  ){}

  canActivate(): boolean {
    const token = this._authService.getToken();
    if (!token) {
      this._router.navigate(['admin/login']).then(() => {
        return false;
      });
    }
    return true;
  }
}